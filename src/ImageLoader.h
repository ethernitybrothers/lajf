/*
 * ImageLoader.h
 *
 *  Created on: 22. 5. 2015
 *      Author: pjf
 */

#ifndef IMAGELOADER_H_
#define IMAGELOADER_H_

#include "ImageLoader.h"

#include <string>
#include <pthread.h>

class ImageLoader {
public:
    ImageLoader();
    virtual ~ImageLoader();

    void watchFilename(const std::string &filename);

    void start();
    void stop();

    void copyPixels(void *&buffer, int &width, int &height);
    void loadImage();

private:
    int width;
    int height;
    std::string filename;
    void *buffer;
    pthread_mutex_t mutex;
    pthread_t thread;
    int frameNo;
    volatile bool running;

    void run();

    static void * pthreadfunc(void *arg);
};

#endif /* IMAGELOADER_H_ */
