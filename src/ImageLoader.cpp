/*
 * ImageLoader.cpp
 *
 *  Created on: 22. 5. 2015
 *      Author: pjf
 */

#include "ImageLoader.h"

#include <SDL.h>
#include <SDL_image.h>

#include <iostream>
#include <unistd.h>

#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

ImageLoader::ImageLoader() {
    running = false;
    pthread_mutex_init(&mutex, 0);
}

ImageLoader::~ImageLoader() {
    if (buffer)
        free(buffer);
    pthread_mutex_destroy(&mutex);
}


void ImageLoader::watchFilename(const std::string &filename) {
    this->filename = filename;
}


void ImageLoader::start() {

    running = true;
    if (pthread_create(&thread, NULL, pthreadfunc, this)) {
        std::cerr << "pthread_create failed" << std::endl;
    }

}

void ImageLoader::stop() {
    running = false;
    pthread_join(thread, NULL);
}

void ImageLoader::run() {

    frameNo = 0;
    while (running) {
        loadImage();
        sleep(2);
        frameNo++;
    }
}

void * ImageLoader::pthreadfunc(void *arg) {
    ((ImageLoader*) arg)->run();
    return NULL;
}


void ImageLoader::copyPixels(void *&buf, int &w, int &h) {
    pthread_mutex_lock(&mutex);
    buf = malloc(4*width*height);
    w = width;
    h = height;
    memcpy(buf, buffer, 4*width*height);
    pthread_mutex_unlock(&mutex);
}

void ImageLoader::loadImage() {
    std::cout << "loading frameNo #" << frameNo << std::endl;
    fs::path curdir("img");
    fs::directory_iterator end_iter;
    std::string selectedfile;
    for( fs::directory_iterator dir_iter(curdir) ; dir_iter != end_iter ; ++dir_iter)
      {
        if (fs::is_regular_file(dir_iter->status()) )
        {
            std::string thisfile = dir_iter->path().generic_string();
            /*if (thisfile.find("22") != 0)
                continue;*/
            // std::cout << thisfile << std::endl;
            if (selectedfile.empty()) {
                selectedfile = dir_iter->path().generic_string();
            }
            else {
                if (selectedfile < thisfile)
                    selectedfile = thisfile;
                // result_set.insert(result_set_t::value_type(fs::last_write_time(dir_iter->status()), *dir_iter);
                // std::cout << selectedfile << std::endl;
            }
        }
      }

    SDL_Surface *image = IMG_Load(selectedfile.c_str());
    if(!image) {
        std::cerr << "IMG_Load: " << IMG_GetError() << std::endl;
        return;
    }
    SDL_Surface *convertedImage = SDL_ConvertSurfaceFormat(image, SDL_PIXELFORMAT_RGBA8888, 0);
    SDL_FreeSurface(image);

    pthread_mutex_lock(&mutex);
    width = convertedImage->w;
    height = convertedImage->h;
    if (!buffer)
        buffer = malloc(4*width*height);

    memcpy(buffer, convertedImage->pixels, 4*width*height);
    pthread_mutex_unlock(&mutex);
    SDL_FreeSurface(convertedImage);

    std::cout << "selected file: " << selectedfile << std::endl;
}
