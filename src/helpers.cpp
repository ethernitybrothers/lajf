
/*
void render_gui() {
    // make sure that before calling renderAllGUIContexts, that any bound textures
    // and shaders used to render the scene above are disabled using
    // glBindTexture(0) and glUseProgram(0) respectively also set
    // glActiveTexture(GL_TEXTURE_0)

    // glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);
    glActiveTexture(0);

    // draw GUI
    // NB: When not using >=3.2 core profile, this call should not occur
    // between glBegin/glEnd calls.
    CEGUI::System::getSingleton().renderAllGUIContexts();
}

GLuint load_texture(const char* fileName) {
    SDL_Surface *image = IMG_Load(fileName);
    SDL_Surface *imagergba = SDL_ConvertSurfaceFormat(image, SDL_PIXELFORMAT_RGBA8888, 0);
    SDL_FreeSurface(image);

    // The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
    GLuint framebufferName = 0;
    glGenFramebuffers(1, &framebufferName);
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferName);

    // The texture we're going to render to
    GLuint renderedTexture;
    glGenTextures(1, &renderedTexture);

    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, renderedTexture);

    // Give an empty image to OpenGL ( the last "0" )
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imagergba->w, imagergba->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, imagergba->pixels);

    // Poor filtering. Needed !
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // Set "renderedTexture" as our colour attachement #0
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

    // Set the list of draw buffers.
    GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers



    GLuint object;
    glGenTextures(1, &object);
    glBindTexture(GL_TEXTURE_2D, object);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imagergba->w, imagergba->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, imagergba->pixels);
    SDL_FreeSurface(imagergba);
    return object;
}

GLuint tex;

void render_lajf() {
    glClearColor(0, 0.0, 0.0, 0.0);
    glViewport(0, 0, screen_width, screen_height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 0, screen_width, screen_height);
    // glDisable(GL_DEPTH_TEST);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // glColor3f(1.0, 1.0, 1.0);
    glBindTexture(GL_TEXTURE_2D, tex);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex2f(0, 0);
    glTexCoord2f(1, 0);
    glVertex2f(screen_width, 0);
    glTexCoord2f(1, 1);
    glVertex2f(screen_width, screen_height);
    glTexCoord2f(0, 1);
    glVertex2f(0, screen_height);
    glEnd();
    glFlush();
}
*/
